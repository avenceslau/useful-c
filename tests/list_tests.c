#include <stdio.h>

#include "list_tests.h"
#include "dl_list.h"

#define DL_LIST_TEST(test_name, comparison, ...) TEST(DL_LIST_SUITE ,test_name, comparison, __VA_ARGS__)

int correct_inserts(dl_list l, object **objects, int expected_objs) {
    int counter = 0;
    for (int i = 0; i < expected_objs ; i++) {
        if(dl_add_item(l, wrapper_example(objects[i])))
            counter++;
    }
    return counter;
}

void remove_items_empty_list(dl_list l, object **objects, int expected_objs) {
    DL_LIST_TEST("Remove head (empty list)", unwrapper_example(dl_remove_head(l)) == NULL, "Return should be NULL");
    DL_LIST_TEST("Remove tail (empty list)", unwrapper_example(dl_remove_tail(l)) == NULL, "Return should be NULL");
    DL_LIST_TEST("Remove with key empty list/not existent key", unwrapper_example(dl_remove_item(1, l)) == NULL, "Should be NULL");
}

void test_remove_edge_case(const char * test_name,dl_list l, object **objects, const char * message, void * rem1, void * rem2) {
    DL_LIST_TEST(test_name, ((rem1 != NULL) && (rem2 != NULL)), message);
}

void insert_two_objects(dl_list l, object **objects) {
    ASSERT(dl_add_item(l, objects[0]));
    ASSERT(dl_add_item(l, objects[1]));
}

void remove_items_test(dl_list l, object **objects, int expected_objs) {
    int counter = 0;
    for (int i = 0 ; i < expected_objs ; i++) {
        void * aux = dl_remove_head(l);
        if (aux != NULL) {
            counter++;
        }
    }
    DL_LIST_TEST("Consecutive removes", (counter == expected_objs), "Only removed %d of %d", counter, expected_objs);
    
    insert_two_objects(l, objects);
    test_remove_edge_case("Remove all items from head", l, objects, "Should have received two objs", dl_remove_head(l), dl_remove_head(l));
    
    
    insert_two_objects(l, objects);
    test_remove_edge_case("Remove all items from tail", l, objects, "Should have received two objs", dl_remove_tail(l), dl_remove_tail(l));
    
    insert_two_objects(l, objects);
    test_remove_edge_case("Remove head using key", l, objects, "Should have received two objs", dl_remove_item(0, l), dl_remove_head(l));
    
    insert_two_objects(l, objects);
    test_remove_edge_case("Remove tail using key", l, objects, "Should have received two objs", dl_remove_item(1, l), dl_remove_tail(l));
    
    insert_two_objects(l, objects);
    ASSERT(dl_add_item(l, objects[2]));
    object * aux = unwrapper_example(dl_remove_item(1, l));
    int got = (int) get_key(dl_remove_head(l));
    DL_LIST_TEST("Remove tail using key", ((aux != NULL) && (got == get_key(wrapper_example(objects[0])))), "Expected %d got %d", get_key(wrapper_example(objects[0])), got);
    
}

void dl_list_tests(int number_successful_objects, object ** objects, const char * dl_list_suite_name, int expected_objs) {
    int counter = 0;
    int key = 0;
    SUITE(DL_LIST_SUITE);
    
    DL_LIST_TEST("Confirmation of object creation", number_successful_objects == expected_objs, "Expected %d objs got %d objs", expected_objs, number_successful_objects);
    
    dl_list l = dl_create(free_object, get_key, print_item);
    DL_LIST_TEST("DList Creation", l != NULL, "List Creation failed");
    remove_items_empty_list(l, objects, expected_objs);
    counter = correct_inserts(l, objects, expected_objs);
    DL_LIST_TEST("Add objects to list", counter == expected_objs, "Failed to add %d only added %d", expected_objs, counter);
    key = 0;
    void * aux = dl_find_item(0, l);
    DL_LIST_TEST("Find object (head)",  get_key(aux) == get_key(wrapper_example(objects[key])), "Expected %d got %d", get_key(wrapper_example(objects[key])), get_key(aux));
    key = expected_objs-1;
    aux = dl_find_item(key, l);
    DL_LIST_TEST("Find object (tail)",  get_key(aux) == get_key(wrapper_example(objects[key])), "Expected %d got %d", get_key(wrapper_example(objects[key])), get_key(aux));
    key = expected_objs-3;
    aux = dl_find_item(key, l);
    DL_LIST_TEST("Find object (middle)",  get_key(aux) == get_key(wrapper_example(objects[key])), "Expected %d got %d", get_key(wrapper_example(objects[key])), get_key(aux));
    remove_items_test(l, objects, expected_objs);
    dl_free(&l, false);
    DL_LIST_TEST("Free list", (l == NULL), "l should be NULL");
}

