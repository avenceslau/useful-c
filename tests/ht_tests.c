//
//  ht_tests.c
//
//  Created by André Venceslau on 15/08/2021.
//

#include "ht_tests.h"
#include "hash_table.h"
#include "tester.h"

#define HT_TEST(testname, comparison, ...) TEST(HT_SUITE, testname, comparison, __VA_ARGS__)

void ht_tests(int number_successful_objects, object ** objects, int expected_objs, const char * suite_name) {
    SUITE(HT_SUITE);
    ht table = ht_create(10, free_object);
    ht aux = NULL;
    char * key1 = "Key 1";
    HT_TEST("HT Creations", table != NULL, "Table should be created");
    
    HT_TEST("HT insert", ht_insert(&table, key1, objects[0]) == true, "Shoul have been able to insert");
    HT_TEST("HT search for a key", ht_search(table, key1) != NULL, "Should find");
    HT_TEST("HT delete key", ht_delete_key(table, key1) != NULL, "Should be able to delete");
    HT_TEST("HT delete non existing key", ht_delete_key(table, key1) == NULL, "Should not be able to delete");
    HT_TEST("HT search on empty table", ht_search(table, key1) == NULL, "Should not find");
    
    
    HT_TEST("HT delete non existing key", ht_delete_key(aux, key1) == false, "");
    HT_TEST("HT search on non existent table", ht_search(aux, key1) == NULL, "Should not find");
    HT_TEST("HT add to non existing table", ht_insert(&aux, key1, objects[0]) == false, "Should not be able to insert");
    
    HT_TEST("HT insert", ht_insert(&table, key1, objects[0]), "Should be able to insert");
    HT_TEST("HT search", ht_search(table, key1), "Should have found object");
    HT_TEST("HT delete key", ht_delete_key(table, key1) != NULL, "Should be able to delete");
    HT_TEST("HT search deleted key", ht_search(table, key1) == false, "Should not be able to find");
}
