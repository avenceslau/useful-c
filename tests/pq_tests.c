//
//  pq_tests.c
//
//  Created by André Venceslau on 14/08/2021.
//

#include "pq_tests.h"

void pq_tests(int number_successful_objects, object ** objects, int expected_objs, const char * suite_name, bool is_min_queue) {
    int counter = 0;
    SUITE(suite_name);
    pq q = pq_create(10, is_min_queue, print_item, get_key);
    TEST(suite_name, "MIN PQ creation", q != NULL, "Creation failed");
    for (int i = 0; i < expected_objs ; i++) {
        if(pq_add(q, wrapper_example(objects[i])))
            counter++;
    }
    TEST(suite_name, "MIN PQ bulk insert", counter == expected_objs, "Expected inserts %d got %d", expected_objs, counter);
    TEST(suite_name, "Verify MIN PQ condition", is_heap(q) == false, "Should be min heap");
    counter = 0;
    TEST(suite_name, "MIN PQ Check top of PQ", pq_check_top(q) != NULL, "Should return an object");
    for (int i = 0; i < expected_objs ; i++) {
        if(pq_remove_top(q) != NULL)
            counter++;
    }
    TEST(suite_name, "MIN PQ bulk remove", counter == expected_objs, "Expected removes %d got %d", expected_objs, counter);
    TEST(suite_name, "Remove from empty queue", pq_remove_top(q) == NULL, "Should not remove anything");
    TEST(suite_name, "Check top of empty PQ", pq_check_top(q) == NULL, "Should return NULL");
    pq_free(&q);
    TEST(suite_name, "Free PQ", q == NULL, "PQ should be NULL");
    TEST(suite_name, "Add to non PQ", pq_add(q, objects[0]) == false, "Insertion should fail");
    TEST(suite_name, "Remove from non existent queue", pq_remove_top(q) == NULL, "Should not remove anything");
    TEST(suite_name, "Check top of non existent PQ", pq_check_top(q) == NULL, "Should return NULL");
    
}
