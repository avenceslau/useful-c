//
//  pq_tests.h
//
//  Created by André Venceslau on 14/08/2021.
//

#ifndef pq_tests_h
#define pq_tests_h

#ifdef __cplusplus
extern "C" {
#endif

#include <stdio.h>
#include "object.h"
#include "tester.h"
#include "PQueue.h"

#define MIN_PQ_SUITE "MIN PQ (Priority Queue) tests"
#define MAX_PQ_SUITE "MAX PQ (Priority Queue) tests"

void pq_tests(int number_successful_objects, object ** objects, int expected_objs, const char * suite_name, bool is_min_queue);

#ifdef __cplusplus
}
#endif
#endif /* pq_tests_h */
