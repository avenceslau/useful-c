//
//  tester.c
//
//  Created by André Venceslau on 12/08/2021.
//
#ifndef list_tests_h
#define list_tests_h

#ifdef __cplusplus
extern "C" {
#endif

#include <stdio.h>
#include "object.h"
#include "tester.h"

#define DL_LIST_SUITE "dl_list (Double Linked List) tests"

void dl_list_tests(int number_successful_objects, object ** objects, const char * dl_list_suite_name, int expected_objs);

#ifdef __cplusplus
}
#endif

#endif /* list_tests_h */
