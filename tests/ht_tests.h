//
//  ht_tests.h
//
//  Created by André Venceslau on 15/08/2021.
//

#ifndef ht_tests_h
#define ht_tests_h

#include <stdio.h>
#include "object.h"

#define HT_SUITE "HT (Hash Table) tests"

void ht_tests(int number_successful_objects, object ** objects, int expected_objs, const char * suite_name);

#endif /* ht_tests_h */
