//
//  pqueue.c
//  Projeto-Aed
//
//  Created by André Venceslau on 03/12/2020.
//

#include "PQueue.h"

struct pq {
    void ** data;
    unsigned long count;
    unsigned long size;
    bool is_min_queue;
    unsigned long (*get_priority)(void *);
    void (*print_item)(void *);
    void (*free_item)(void *);
};



bool _compare(void * item1, void * item2, pq q){
    bool res;
    //compare for max
    res = (q->get_priority(item1) < q->get_priority(item2));
    return (q->is_min_queue) ? !res : res;
}

bool pq_free(pq  * q){
    if (q == NULL)
        return false;

    free((*q)->data);
    free(*q);
    *q = NULL;
    return true;
}

bool pq_print(pq q){
    if (q == NULL)
        return false;
    
    for (int i = 0; i < q->count ; i++) {
        q->print_item(q->data[i]);
    }
    return true;
}

/**
 * @brief creates a priority queue with size
 * 
 * @param size size of the priority queue
 * @param is_min_queue if true is min queue
 * @param print_item rules on how to print the item
 * @param get_priority rules on how to get the priority of an item
 * 
 * Todo implement case where is_min_queue equals false
 * @return pq* 
 */
pq pq_create(unsigned long size, bool is_min_queue, void (*print_item)(void *), unsigned long (*get_priority)(void *)){
    pq q = (pq) malloc(sizeof(struct pq));
    if (q == NULL) {
        return NULL;
    }
    
    q->count = 0;
    q->size = size;
    q->is_min_queue = is_min_queue; 
    q->get_priority = get_priority;
    q->print_item = print_item;
    q->data = (void **) malloc(sizeof(void *)*size); 
    if (q->data == NULL) {
        free(q);
        return NULL;
    }
    for (unsigned long i = 0; i < q->size; i++) {
        q->data[i] = NULL;
    }
    
    return q;
}

bool FixUp(pq q, unsigned long index) {
    if (q == NULL)
        return false;
    
    void * t;
    unsigned long i = index;
    while ((i > 0) && _compare(q->data[(i - 1) / 2], q->data[i], q)) {
        t = q->data[i];
        q->data[i] = q->data[(i - 1) / 2];
        q->data[(i - 1) / 2] = t;
        i = (i - 1) / 2;
    }
    return true;
}

bool FixDown(pq q, unsigned long index) {
    if (q == NULL)
        return false;
    
    unsigned long j;
    void * t;
    unsigned long k = index;
    while ((2 * k + 1) < q->count) {
        j = 2 * k + 1;
        if (((j + 1) < q->count) && _compare(q->data[j], q->data[j + 1], q)) {
            /* second offspring is smaller */
            j++;
        }
        if (!_compare(q->data[k], q->data[j], q)) {
            /* Elements are in correct order. */
            break;
        }
        /* the 2 elements are not correctly sorted, it is
         necessary to exchange them */
        t = q->data[k];
        q->data[k] = q->data[j];
        
        q->data[j] = t;
        k = j;
    }
    return true;
}

bool heap_sort(pq q) {
    if (q == NULL)
        return false;
    
    void * aux;
    unsigned long count = q->count;
    for (unsigned long i = q->count-1 ; i > 0 ; i--) {
        aux = q->data[0];
        
        q->data[0] = q->data[i];
        q->data[i] = aux;
        q->count--;
        (void) FixDown(q, 0);
    }
    q->count = count;
  return true;
}

bool pq_add(pq q, void * item){
    if (q == NULL) {
        return false;
    }
    
    q->data[q->count] = item;
    q->count++;
    FixUp(q, q->count-1);
    return true;
}

void * pq_remove_top(pq q) {
    if (q == NULL)
        return NULL;
    ;
    void * tmp = NULL;
    if (q->count > 0) {
        tmp = q->data[0];
        q->data[0] = q->data[q->count - 1];
         
        q->data[q->count - 1] = NULL;
        q->count--;
        FixDown(q, 0);
    }
    return tmp;
}

void * pq_check_top(pq q){
    if (q == NULL) {
        return NULL;
    }
    
    return q->data[0];
}

bool is_heap(pq q){
    if (q == NULL)
        return false;
    
    for (int i=0; i <= (q->count-2)/2; i++) {
        if (_compare(q->data[2*i +1], q->data[i], q))
                return false;
 
        if (2*i+2 < q->count && _compare(q->data[2*i+2], q->data[i], q))
                return false;
    }
    return true;
}
