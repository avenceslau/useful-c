#include <stdio.h>
#include <stdlib.h>
#include "object.h"

void * wrapper_example(object * unwrapped_item) {
    return ((void *) unwrapped_item); 
}

object * unwrapper_example(void * wrapped_object){
    return (object *) wrapped_object;
}

char * gen_name(){
    return NULL;
}

unsigned long get_key(void * myobject){
    if (myobject == NULL) {
        return 0;
    }
    return ((object *) myobject)->key;
}

object * create_object(unsigned long key){
    object * created_object = (object *) malloc(sizeof(object));
    created_object->name = gen_name();
    created_object->value = 1;
    created_object->key = key;
    return created_object; 
}

void * print_item(void * item){
    if (item == NULL) {
        return NULL;
    }
    printf("%3ld/%3ld ", unwrapper_example(item)->key, unwrapper_example(item)->key);
    return item;
}

void free_object(void ** object_to_free) {
    if (*object_to_free == NULL) {
        return;
    }
    object * obj = unwrapper_example(*object_to_free);
    if (obj->name != NULL) {
        free(obj->name);
    }
    
    free(obj);
    obj = NULL;
    *object_to_free = (void *) obj;
}

