//
//  Sets.h
//
//  Created by André Venceslau on 10/12/2020.
//

#ifndef Sets_h
#define Sets_h

#include <stdio.h>
#include <stdbool.h>

int findwcompression(int x, int*set);
void SetUnion(int x, int y, int * set, int * sz);
void linkset(int x, int y, int * set);
void makeset(int x, int*set, int *sz);
void makesetnosize(int x, int*set);
int findsize(int x, int *sz, int * set);

#endif /* Sets_h */
