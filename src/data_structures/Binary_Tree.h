//
//  Binary_Tree.h
//
//  Created by André Venceslau on 25/10/2020.
//

#ifndef Binary_Tree_h
#define Binary_Tree_h

#ifdef __cplusplus
extern "C" {
#endif

#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include "Binary_Tree.h"

typedef struct avl_tree avl_tree;

void avl_free(avl_tree * tree);
int avl_get_height(avl_tree *n);
unsigned long avl_get_number_nodes(avl_tree * tree);
avl_tree * avl_create(unsigned long (*get_key)(void *));
bool avl_insert(avl_tree ** tree, void * item);
void * avl_find(avl_tree * tree, unsigned long key);
void avl_get_every_item(avl_tree * tree, void *array[], int *i);

#ifdef __cplusplus
}
#endif

#endif /* Binary_Tree_h */
