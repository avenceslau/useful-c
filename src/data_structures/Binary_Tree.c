//
//  Binary_Tree.c
//
//  Created by André Venceslau on 25/10/2020.
//

#include "Binary_Tree.h"
#include <stdlib.h>

#define max(a, b) ((a > b) ? a : b)

typedef struct _node node;

struct _node{
    void * item; 
    int height; 
    node * left; 
    node * right;
};

struct avl_tree {
    unsigned long n_nodes;
    unsigned long (*get_key)(void *);
    node * root;
};


void avl_free_internal(node * root) {
    if(root != NULL){
        if(root->left != NULL)
            avl_free_internal(root->left);
        if(root->right != NULL)
            avl_free_internal(root->right);
        free(root);
    }
}

void avl_free(avl_tree * tree){
    avl_free_internal(tree->root);
    free(tree);
}

int height(node * root){
    return root->height;
}

int avl_get_height(avl_tree *n) {
    if (n == NULL){
        return 0;
    }
    return height(n->root);
}

unsigned long avl_get_number_nodes(avl_tree * tree){
    return tree->n_nodes;
}

node * create_node(void * item) {
    if (item == NULL) {
        return NULL;
    }
    
    node * new = (node *) malloc(sizeof(node));
    if (new == NULL) {
        return NULL;
    }
    new->left = NULL;
    new->right = NULL;
    new->item = item;
    new->height = 1;
    return new;
}

int balance(node * root) {
    if(root == NULL)
        return 0;
    
    return height(root->left)-height(root->right);
}

avl_tree * avl_create(unsigned long (*get_key)(void *)){
    avl_tree * tree = (avl_tree *) malloc(sizeof(avl_tree));
    if (tree == NULL) {
        return NULL;
    }
    tree->n_nodes = 0;
    tree->get_key = get_key;
    tree->root = NULL;  
    return tree;
}

node * avl_rotate_right(node * y){
    node * x = y->left;
    node * z = x->right;
    
    x->right = y;
    y->left = z;
    
    y->height = 1 + max(height(y->left), height(y->right));
    x->height = 1 + max(height(x->left), height(x->right));
    
    return x;
}

node * avl_rotate_left(node * x){
    node * y = x->right;
    node * z = y->left;
    
    y->left = x;
    x->right = z;
    
    x->height = 1 + max(height(x->left), height(x->right));
    y->height = 1 + max(height(y->left), height(y->right));
    
    return y;
}

node * avl_insert_internal(node * root, node * new_node, avl_tree * tree){
    if (root == NULL) { 
        return new_node; 
    }

    if (tree->get_key(new_node->item) < tree->get_key(root->item)) {
        root->left  = avl_insert_internal(root->left, new_node, tree);
        
    } else if (tree->get_key(new_node->item) > tree->get_key(root->item)) {
        root->right  = avl_insert_internal(root->right, new_node, tree);
        
    } else { 
        return root; 
    }
    
    root->height = 1 + max(height(root->left), height(root->right));
    int bal = balance(root);
    
    if(bal >  1 && tree->get_key(new_node->item) < tree->get_key(root->left->item))  { 
        return avl_rotate_right(root);
    } else if(bal < -1 && tree->get_key(new_node->item) > tree->get_key(root->right->item)) {
        return avl_rotate_left(root); 
    } else if(bal >  1 && tree->get_key(new_node->item) > tree->get_key(root->left->item))  {
        root->left = avl_rotate_left(root->left);
        return avl_rotate_right(root);
    } else if(bal < -1 && tree->get_key(new_node->item) < tree->get_key(root->right->item)) {
        root->right = avl_rotate_right(root->right);
        return avl_rotate_left(root);
    }
    return root;
}

bool avl_insert(avl_tree ** tree, void * item) {
    node * new = create_node(item);
    if (new == NULL) {
        return false;
    }
    (*tree)->root = avl_insert_internal((*tree)->root, new, *tree);
    (*tree)->n_nodes++;
    return true;
}


//Vê-se numa árvore existe uma ligação a um node "name" name = id do node
void * avl_find_internal(node * root, unsigned long key, avl_tree * tree){
    if(root == NULL){
        return NULL;
    }
    
    if(key == tree->get_key(root)){
        return root->item;
    } else if(key < tree->get_key(root)){
        return avl_find_internal(root->left, key, tree);
    } else if (key > tree->get_key(root)) {
        return avl_find_internal(root->right, key, tree);
    } else {
        return NULL;
    }
}

void * avl_find(avl_tree * tree, unsigned long key) {
    return avl_find_internal(tree->root, key, tree);
}

//Recebe um array x posições onde x = n_links de uma árvore
//guarda em cada posição do array um dos nodes
void avl_get_every_item_internal(node * root, void *array[], int *i){
    if(!root){
        return;
    }
    array[(*i)] = root->item;
    (*i)++;
    if (!(root->left) && !(root->right) ) { //Dead Ends
        return;
    }
    
    if (root->left) {
        avl_get_every_item_internal(root->left,array,i);
    }
    if (root->right) {
        avl_get_every_item_internal(root->right, array, i);
    }
}

void avl_get_every_item(avl_tree * tree, void *array[], int *i){
    avl_get_every_item_internal(tree->root, array, i);
}
