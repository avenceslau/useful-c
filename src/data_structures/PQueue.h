//
//  PQueue.h
//  Projeto-Aed
//
//  Created by André Venceslau on 03/12/2020.
//

#ifndef PQueue_h
#define PQueue_h

#ifdef __cplusplus
extern "C" {
#endif

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

typedef struct pq * pq;

pq pq_create(unsigned long size, bool is_min_queue, void (*print_item)(void *), unsigned long (*get_priority)(void *));
bool pq_add(pq q, void * item);
void pq_modify(pq q, int i, unsigned long newcost);
bool pq_free(pq * q);
void * pq_check_top(pq q);
bool heap_sort(pq q);
void * pq_remove_top(pq q);
bool pq_print(pq q);
bool is_heap(pq q);

#ifdef __cplusplus
}
#endif

#endif /* PQueue_h */
