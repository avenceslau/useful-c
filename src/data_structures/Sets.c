//
//  Sets.c
//
//  Created by André Venceslau on 10/12/2020.
//

#include "Sets.h"
#include <stdlib.h>

typedef struct set set;

struct set {
    int * set;
    int * sizes;
    int set_size; 
};


void set_init(int x, int*set, int *sz){
    if (set[x] == 0) {
        set[x] = x;
        sz[x] = 1;
    }
}

void set_init_nosize(int x, int*set){
    if (set[x] == 0) {
        set[x] = x;
    }
}

/**
 * @brief Create a disjoint set object the set is initialized               
 * 
 * @param set_size the size of the set
 * @param use_sizes if true has an array of sizes
 * @return set* a ser pointer
 */
set * set_create(int set_size, bool use_sizes){
    set *new_set = (set *) malloc(sizeof(set));
    if (new_set == NULL) {
        return NULL;
    }
    new_set->sizes = NULL;
    new_set->set = (int *) calloc(set_size, sizeof(int));
    if (new_set->set == NULL)
    {
        free(new_set);
        return NULL;
    }
    new_set->set_size = set_size;
    if (use_sizes) {
        new_set->sizes = (int *) calloc(set_size, sizeof(int));
        if (new_set->sizes == NULL) {
            free(new_set->set);
            free(new_set);
            return NULL;
        }
        for (int i = 0; i < set_size; i++) {
            set_init(i, new_set->set, new_set->sizes);    
        }    
    } else {
        for (int i = 0; i < set_size; i++) {
            set_init_nosize(i, new_set->set);    
        }        
    }
    return new_set;
    
}

/**
 * @brief Find the set representative of a value, compresses the path
 * 
 * @param x value
 * @param set set
 * @return int 
 */
int set_findwcompression(int x, int*set){
    int root = x;
    while (root != set[root]) {
        root = set[root];
    }
    while (x != root) {
        int next = set[x];
        set[x] = root;
        x = next;
    }
    return root;
}

void set_union(int x, int y, int * set, int * sz){
    x = set_findwcompression(x, set);
    y = set_findwcompression(y, set);
    
    if (x == y) {
        return;
    }
    if (sz[x] < sz[y]) {
        set[x] = y;
        sz[y] = sz[x] + sz[y];
    } else {
        set[y] = x;
        sz[x] = sz[x] + sz[y];
    }
}

void set_link(int x, int y, int * set){
    x = set_findwcompression(x, set);
    y = set_findwcompression(y, set);
    if (x == y) {
        return;
    }
    set[y] = x;
}

int set_findsize(int x, int *sz, int * set){
    return sz[set_findwcompression(x, set)];
}
