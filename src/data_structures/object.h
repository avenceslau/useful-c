#ifndef object_h
#define object_h

#include <stdio.h>
#include <stdbool.h>

typedef struct object object;

struct object {
    int value;
    unsigned long key;
    char * name;
};

char * gen_name(void);
unsigned long get_key(void * myobject);
object * create_object(unsigned long key);
void * print_item(void * item);
void free_object(void ** object_to_free);
void * wrapper_example(object * unwrapped_item);
object * unwrapper_example(void * wrapped_object);

#endif
