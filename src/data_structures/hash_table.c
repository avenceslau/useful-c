//
//  hash_table.c
//
//  Created by André Venceslau on 26/05/2021.
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <math.h>
#include "hash_table.h"

typedef struct hash_item hash_item;

struct ht {
    hash_item** items;
    void (*free_item)(void **);
    unsigned long size;
    unsigned long count;
};

struct hash_item {
    char* key;
    void * value;
    struct hash_item * next;
};

unsigned long find_next_size(unsigned long previous_size);

/**
 * @brief does the malloc() of a string and returns error
 *
 * @param result allocated string passed as reference
 * @param str string
 * @return 0 or error
 */
bool alloc_string(char ** result, const char * str){
    *result = (char *) malloc(strlen(str)+1);
    if (result == NULL) {
        return false;
    }
    strcpy(*result, str);
    return true;
}


/**
 * @brief calculates the index for a key
 * 
 * @param key   key to be hashed
 * @param ht_size size of the hash table
 * @return unsigned long index associated with a key
 */
unsigned long hash_function(const char* key, unsigned long ht_size) {
    unsigned long i = 0;
    if(ht_size == 0)
        return 0;
    for (int j = 0; j < strlen(key); j++) {
        i += 650 * j + key[j];
        if (j == 0)
            i += 550 + key[j];
    }
    return i % ht_size;
}

hash_item * create_item(const char * key, void * value){
    hash_item * item = (hash_item *) malloc(sizeof(hash_item));
    if (item == NULL) {
        return NULL;
    }
    if (alloc_string(&item->key, key) == false) {
        return NULL;
    }
    item->value = value;
    item->next = NULL;
    return item;   
}

void free_item(hash_item * item, ht ht, bool free_contents){
    if (free_contents)
        ht->free_item(&item->value);
    free(item);    
}

/**
 * @brief Create a hash table object
 * 
 * @param min_size size of the hash table
 * @param free_item pointer to a function that says how a item is freed
 * @return ht* a hash table
 */
ht ht_create(unsigned long min_size, void (*free_item)(void **)) {
    // Creates a new HashTable
    ht table = (ht) malloc(sizeof(struct ht));
    if (table == NULL)
        return NULL;
    
    table->size = find_next_size(min_size);
    table->count = 0;
    table->free_item = free_item;
    table->items = (hash_item**) calloc(table->size, sizeof(hash_item*));
    if (table->items == NULL) {
        free(table);
        return NULL;
    }
    
    for (int i = 0; i < table->size; i++)
        table->items[i] = NULL;

    return table;
}

/**
 * @brief frees a table if free_contents is set to true frees all items inside else only frees the ht
 * 
 * @param table pointer to hash table
 * @param free_contents if true will free the objects within the hash_table
 */
void ht_free(ht * table, bool free_contents) {
    // Frees the table
    for (int i = 0; i < (*table)->size; i++) {
        hash_item * item = (*table)->items[i];
        if (item != NULL)
            while (item != NULL) {
                hash_item * aux;
                aux = item->next;
                free_item(item, (*table), free_contents);
                
                item = aux;
            }
    }
    free((*table)->items);
    free(*table);
    *table = NULL;
}

/**
 * @brief deletes an item associated with a key
 * 
 * @param table pointer to a hash table
 * @param key key associated with the item
 * @return int the object on a success on fail returns NULL
 */
void * ht_delete_key(ht table, const char * key){
    void * tmp; //returns the item associated with a key
    hash_item * item = NULL; //serves as list head
    hash_item * prev = NULL; //saves previous node
    hash_item * aux = NULL;
    unsigned long index;
    
    if (table == NULL || key == NULL)
        return NULL;
    
    index = hash_function(key, table->size);
    item = table->items[index];
    
    if (item == NULL)
        return NULL;
    
    if (strcmp(key, item->key) == 0) {
        table->items[index] = item->next;
        tmp = item->value;
        free_item(item, table, false);
        return tmp;
    }
    aux = item;
    while (aux != NULL && (strcmp(key, aux->next->key) == 0)) {
        prev = aux;
        aux = aux->next;
    }
    if (aux == NULL) {
        return NULL;
    }
    prev->next = aux->next;
    tmp = item->value;
    free_item(item, table, false);
    return tmp;
}

/**
 * @brief resolves a collision, on a collision the item is inserted on a list associated with the index
 * 
 * @param table pointer to hash table
 * @param index index of where the collision happened
 * @param item the item that is being inserted
 */
void handle_collision(ht table, unsigned long index, hash_item* item) {
    hash_item * runner = table->items[index];
    while (item->next != NULL) {
        //if key already exists update value
        if (strcmp(runner->key, item->key) == 0) {
            free(runner->value);
            runner->value = item->value;
            free(item->key);
            free(item);
            return;
        }
    }
    item->next = table->items[index];
    table->items[index] = item;
    return;
}

unsigned long find_next_size(unsigned long previous_size){
    unsigned long next_size = previous_size + HT_INITIAL_SIZE;
    unsigned long * primes = (unsigned long *) malloc(sizeof(unsigned long)*next_size);
    if (primes == NULL) {
        printf("Failed malloc\n");
        return 0;
    }
    for (unsigned long i = 0 ; i < 3*next_size ; i++) {
        primes[i] = 0;
    }
    unsigned long limit = sqrt(next_size) + 1;
    for (int i=2; i<limit; i++) {
        if (primes[i-1]) {
            for (int j=i*i; j<=next_size; j+=i) {
                primes[j-1] = 0;
            }
        }
    }
    
    unsigned long new_size = 0;
    for (unsigned long i=next_size; i >= 2 ; i--) {
        if (primes[i]) {
            new_size = i;
            break;
        }
    }
    free(primes);
    return new_size;
}

/**
 * @brief resizes the hash table to accomodate more items
 * 
 * @param table pointer to hash table that will be resized
 * @return int returns a 0 on success or an error on a failed
 */
bool ht_resize(ht * table) {
    unsigned long next_size = find_next_size((*table)->size);
    ht new_table = ht_create(next_size, (*table)->free_item);
    hash_item * item;
    if (new_table == NULL)
       return false;
    
    for (int i = 0; i < (*table)->size; i++) {
        item = (*table)->items[i];
        while (item != NULL){
            if(!ht_insert(&new_table, item->key, item->value)){
                ht_free(&new_table, false);
                return false;
            }
            item = item->next;
        }

    }
    ht_free(table, false);
    (*table) = new_table;
    return true;
}

/**
 * @brief inserts a key value pair in the hash table
 * 
 * @param table pointer to hash table
 * @param key key to accesss value
 * @param value value to be insert
 * @return int 
 */
bool ht_insert(ht * table, const char* key, void* value) {
    if (*table == NULL || table == NULL) {
        return false; 
    }
    if ((*table)->count == (*table)->size){
        if(!ht_resize(table))
            return false;
    }

    hash_item* item = create_item(key, value);
    if (item == NULL)
        return false;
    
    unsigned long index = hash_function(key, (*table)->size);

    hash_item* current_item = (*table)->items[index];

    if (current_item == NULL) {        
        (*table)->items[index] = item;
        (*table)->count++;
        return true;
    } else {
        // Scenario 1: We only need to update value
        if (strcmp(current_item->key, key) == 0) {
            (*table)->items[index]->value = value;
            return true;
        } else {
            handle_collision(*table, index, item);
           (*table)->count++;
           return true;
        }
    }
    return false;
}

/**
 * @brief searches for a value on an hash table associated to a key
 * 
 * @param table pointer to a hash table
 * @param key key used to access a value
 * @return int returns 0 on a success or an error if there is no value or the key doesn't exist and the value is returned if found
 */
void * ht_search(ht table, const char* key) {
    void * value = NULL;
    if (table == NULL) {
        return NULL;
    }
    int index = (int) hash_function(key, table->size);
    hash_item* item = table->items[index];
    
    if (item == NULL){
        return NULL;
    }
    while (item != NULL) {
        if (strcmp(key, item->key) == 0) {
            value = item->value;
            return value;
        }
        item = item->next;
    }
    return NULL;
}

/**
 * @brief returns the number of items in an hash table
 * 
 * @param table pointer to the table
 * @return int returns the nyber of items on an hash table
 */
unsigned long ht_number_of_items(ht table){
    if(table == NULL){
        return 0;
    }
    return table->count;
}

/**
 * @brief returns the hash table size
 * 
 * @param ht pointer to the hash table
 * @return unsigned long 
 */
unsigned long current_size(ht ht){
    if (ht == NULL) {
        return 0;
    }
    return ht->size;
}
