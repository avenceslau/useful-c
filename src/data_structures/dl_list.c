//
//  dl_list.c
//
//  Created by André Venceslau on 26/05/2021.
//

#include "dl_list.h"
#include <stdlib.h>

typedef struct dl_list  * dl_list;
typedef struct list_item list_item;

/**
 * @brief struct defining the properties of a double linked list node
 * 
 */
struct list_item{
    void * item;
    list_item * next;
    list_item * previous;
};

/**
 * @brief struct defining the properties double linked list 
 * 
 */
struct dl_list{
    /* this function is created by the user and frees the item */
    void (*free_item)(void **);
    /* this function is created by the user and finds the item key */ 
    unsigned long (*get_item_key)(void *);
    /* this function is created by the user and prints the item */ 
    void *(*print_item)(void *);
    list_item * head;
    list_item * tail;
};

/**
 * @brief creates a new list item and also creates the item that will be inserted
 * 
 * @param item item that will be added
 * @param l pointer to a list 
 * @return list_item* null on an error or the pointer to the list item
 */
list_item * new_item(void * item, dl_list l){
    if (item == NULL) {
        return NULL;
    }
    
    list_item * new_item = (list_item *) malloc(sizeof(list_item));
    if (new_item == NULL) {
        return NULL;
    }
    new_item->item = item;
    if (new_item->item == NULL) {
        free(new_item);
        return NULL;
    }
    new_item->next = NULL;
    new_item->previous = l->tail;
    return new_item;
}

/**
 * @brief adds an item to the list, handles all the aloocations necessary
 * 
 * @param l pointer to list
 * @param item pointer to the item that will be added shlould be passed as reference
 */
bool dl_add_item(dl_list l, void * item){
    if(item == NULL)
        return false;
    if (l == NULL) {
        return false;
    }
    list_item * item_l = new_item(item, l);
    if (item_l == NULL) {
        return false;
    }
    
    if (l->head == NULL) {
        l->head = item_l; 
        l->tail = l->head;
        return true;
    }
    l->tail->next = item_l;
    l->tail = l->tail->next;
    return true;
}

/**
 * @brief frees a list
 * 
 * @param l pointer to the list
 */
void dl_free(dl_list *l, bool free_contents){
    list_item * runner = (*l)->head;
    while (runner != NULL) {
        if (free_contents == true)
            (*l)->free_item((void **) &runner->item);
        
        runner->item = NULL;
        (*l)->head = runner->next;
        if (runner != NULL) {
            free(runner);
        }
        runner = (*l)->head;
    }
    if ((*l) != NULL) {
        free(*l);
    }
    (*l)->head = NULL;
    (*l)->tail = NULL;
    (*l) = NULL;
}

/**
 * @brief Create a list object
 * 
 * @param free_item rules on how to free the item
 * @param get_key finds the indentifier of an item
 * @param print_item prints the item receibes a void * that countains the item that will be printed
 
 * @return list* returns null on a failure and returs the pointer to the list on a success 
 */
dl_list dl_create(void free_item(void **), unsigned long get_key(void *), void *print_item(void *)){
    dl_list l = (struct dl_list *) malloc(sizeof(struct dl_list));
    if (l == NULL)
        return NULL;
    
    l->free_item = free_item;
    l->get_item_key = get_key;
    l->print_item = print_item;
    l->head = NULL;
    l->tail = NULL;
    return l;
}

/**
 * @brief searches for a list item
 * 
 * @param key identifier of the item
 * @param l pointer to the list
 * @return list_item* returns the list item if found or null on a failure
 */
list_item * dl_find_item_iternal(unsigned long key, dl_list l){
    list_item * runner;
    runner = l->head;
    while (runner != NULL) {
        if (key == l->get_item_key(runner->item)) {
            return runner;
        }
        runner = runner->next;
    }
    return NULL;
}

/**
 * @brief finds the item stored on a list
 * 
 * @param key indentifier of the item
 * @param l pointer to the list
 * @return void* returns the item or null
 */
void * dl_find_item(unsigned long key, dl_list l){
    list_item * aux = dl_find_item_iternal(key, l);
    if (aux == NULL)
        return NULL;
    
    return aux->item;
}

/**
 * @brief removes an item from the list
 * 
 * @param key identifier of the item that wille deleted
 * @param l pointer to the lsit
 * @return void* returns the item that was on the list and removes it, or returns null if not found
 */
void * dl_remove_item(unsigned long key, dl_list l){
    list_item * item = dl_find_item_iternal(key, l);
    list_item * aux = NULL;
    void * res;
    if (item == NULL)
        return NULL;
    if (item == l->tail && item == l->head) {
        l->head = NULL;
        l->tail = NULL;
        res = item->item;
        free(item);
        return res;
    }
    if (item == l->head) {
        if (item->next != NULL) {
            aux = item->next;
            aux->previous = NULL;
        }
        l->head = aux;
        res = item->item;
        free(item);
        return res;
    }
    if (item == l->tail) {
        if (item->previous != NULL) {
            item->previous->next = NULL;
            aux = item->previous;
        }
        l->tail = aux;
        res = item->item;
        free(item);
        return res;
    }
    item->previous->next = item->next;
    item->next->previous = item->previous;
    res = item->item;
    free(item);
    return res;
}

/**
 * @brief removes an item from the list
 *
 * @param l pointer to the lsit
 * @return void* returns the next item on the list and removes the current item, or returns null if not found
 */

list_item * remove_item_from_list_receives_item(list_item * item, dl_list l){
    list_item * aux = NULL;
    void * res;
    if (item == NULL)
        return NULL;
    if (item == l->tail && item == l->head) {
        l->head = NULL;
        l->tail = NULL;
        res = item->item;
        free(item);
        l->free_item(res);
        return NULL;
    }
    if (item == l->head) {
        if (item->next != NULL) {
            aux = item->next;
            aux->previous = NULL;
        }
        l->head = aux;
        res = item->item;
        l->free_item(res);
        return aux;
    }
    if (item == l->tail) {
        if (item->previous != NULL) {
            aux = item->previous;
            aux->next = NULL;
        }
        l->tail = aux;
        res = item->item;
        l->free_item(res);
        return NULL;
    }
    aux = item->next;
    item->previous->next = item->next;
    item->next->previous = item->previous;
    res = item->item;
    l->free_item(res);
    return aux;
}

/**
 * @brief prints the whole list
 * 
 * @param l pointer to the list that will be printed
 */
void dl_print_list(dl_list l){
    if(l == NULL)
        return;
    list_item * runner = NULL;
    void * aux;
    runner = l->head;
    while (runner != NULL) {
        aux = l->print_item(runner->item);
        if (aux == NULL) {
            runner = remove_item_from_list_receives_item(runner, l);
        } else {
        runner = runner->next;
        }
    }
}

/**
 * @brief removes the head from the list and returns the item that was stored 
 * 
 * @param l pointer to list
 * @return void* returns either the item or NULL on an error
 */
void * dl_remove_head(dl_list l){
   if (l == NULL) {
        return NULL;
    }
    if (l->head == NULL) {
        return NULL;
    }

    if (l->tail == l->head) {
        l->tail = NULL;
    }
    void * aux = l->head->item;
    list_item * list_item_struct = l->head;
    l->head = l->head->next;
    if (l->head != NULL) {
        l->head->previous = NULL;
    }
    free(list_item_struct);
    return aux;
}

/**
 * @brief removes the tail from the list and returns the item that was stored
 * 
 * @param l pointer to list 
 * @return void* returns either the item or NULL on an error
 */
void * dl_remove_tail(dl_list l) {
    if (l == NULL) {
        return NULL;
    }
    if (l->tail == NULL) {
        return NULL;
    }

    if (l->tail == l->head) {
        l->head = NULL;
    }
    void * aux = l->tail->item;
    list_item * list_item_struct = l->tail;
    l->tail = l->tail->previous; 
    if (l->tail != NULL) {
        l->tail->next = NULL;
    }
    free(list_item_struct);
    return aux;
}
