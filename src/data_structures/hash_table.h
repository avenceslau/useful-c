//
//  hash_table.c
//
//  Created by André Venceslau on 26/05/2021.
//

#ifndef hash_table_h
#define hash_table_h

#ifdef __cplusplus
extern "C" {
#endif

#define HT_INITIAL_SIZE 1103
#include <stdbool.h>

typedef struct ht * ht;

/**
 * @brief inserts a key value pair in the hash table
 * 
 * @param table pointer to hash table
 * @param key key to access value
 * @param value value to be insert
 * @return int 
 */
bool ht_insert(ht* table, const char* key, void* value);

/**
 * @brief frees a table and all items inside it
 * 
 * @param table pointer to hash table
 */
void ht_free(ht * table, bool free_contents);

/**
 * @brief searches for a value on an hash table associated to a key
 * 
 * @param table pointer to a hash table
 * @param key key used to access a value
 * @return int returns 0 on a success or an error if there is no value or the key doesn't exist and the value is returned if found
 */
void * ht_search(ht table, const char* key);

/**
 * @brief Create a hash table object
 * 
 * @param min_size size of the hash table
 * @param free_item pointer to a function that says how a item is freed
 * @return ht* a hash table
 */
ht ht_create(unsigned long min_size, void (*free_item)(void **));

/**
 * @brief deletes an item associated with a key
 * 
 * @param table pointer to a hash table
 * @param key key associated with the item
 * @return int 1 on a success on fail returns key not found
 */
void * ht_delete_key(ht table, const char * key);

/**
 * @brief returns the number of items in an hash table
 * 
 * @param table pointer to the table
 * @return int returns the number of items on an hash table
 */
unsigned long ht_number_of_items(ht table);

/**
 * @brief returns the hash table size
 * 
 * @param ht pointer to the hash table
 * @return unsigned long 
 */
unsigned long ht_current_size(ht ht);

#ifdef __cplusplus
}
#endif

#endif /* hash_table_h */
