//
//  dl_list.h
//
//  Created by André Venceslau on 26/05/2021.
//

#ifndef dl_list_h
#define dl_list_h

#ifdef __cplusplus
extern "C" {
#endif

#include <stdio.h>
#include <stdbool.h>

typedef struct dl_list * dl_list;

/**
 * @brief finds the if it is stored on the list
 * 
 * @param key identifer of the item
 * @param l pointer to the list
 * @return void* returns the item or null
 */
void * dl_find_item(unsigned long key, dl_list l);

/**
 * @brief removes an item from the list
 * 
 * @param key identifier of the item that will be deleted
 * @param l pointer to the list
 * @return void* returns the item that was on the list and removes it from the list, 
 *         or returns null if not found
 */
void * dl_remove_item(unsigned long key, dl_list l);

/**
 * @brief Create a list object
 * 
 * @param free_item rules on how to free the item
 * @param get_key finds the indentifier of an item
 * @param print_item prints the item receibes a void * that countains the item that will be printed
 
 * @return list* returns null on a failure and returs the pointer to the list on a success 
 */
dl_list dl_create(void free_item(void **), unsigned long get_key(void *), void *print_item(void *));

/**
 * @brief frees a list and all items inside it
 * 
 * @param l pointer to the list
 */
void dl_free(dl_list *l, bool free_contents);

/**
 * @brief adds an item to the list, the item should already be allocated 
 * 
 * @param l pointer to list
 * @param item pointer to the item that will be added
 */
bool dl_add_item(dl_list l, void * item);

/**
 * @brief prints the whole list
 * 
 * @param l pointer to the list that will be printed
 */
void dl_print_list(dl_list l);

/**
 * @brief removes the head from the list and returns the item that was stored 
 * 
 * @param l pointer to list
 * @return void* returns either the item or NULL on an error
 */
void * dl_remove_head(dl_list l);

/**
 * @brief removes the tail from the list and returns the item that was stored
 * 
 * @param l pointer to list 
 * @return void* returns either the item or NULL on an error
 */
void * dl_remove_tail(dl_list l);

#ifdef __cplusplus
}
#endif

#endif /* dl_list_h */
